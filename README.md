### Overview

- repo: gitlab.com/NagaSolo/0701_velosiraptor_1-hackerearth

- July 1st, 2020 craft; all hackerearth codemonk series

### Challenges
- 1.0: Basics of Input/Output -> `DONE`
    -> Read tutorial -> `DONE`
    -> code in `C` -> `DONE`
    -> code in `Python` -> `DONE`
- 2.0: Complexity Analysis
    -> Read tutorial -> `DONE`
- 3.0: Implementations(based on ad-hoc question, and brute-force solution(translate english to code))
    -> Read tutorial -> `DONE`
    -> code in `C` -> `WIP`
    -> code in `Python` -> `DONE`
- 4.0: Operators
    -> Read tutorial -> `WIP`
- 5.0: Basics of bit manipulation
    -> Read tutorial -> `WIP`
    -> code in `C` -> `WIP`
    -> code in `Python` -> `DONE`
- 6.0: Backtracking and recursion
    -> Read Tutorial -> `DONE`
    -> code in C -> `WIP`
    -> code in Python -> `WIP`