""" You are given a string S. Count the number of occurrences of all the digits in the string S.

Input:
First line contains string S.

Output:
For each digit starting from 0 to 9, print the count of their occurrences in the string S. So, print  lines, each line containing 2 space separated integers. First integer i and second integer count of occurrence of i. See sample output for clarification.

Constraints:

"""
input_string = input()
digit_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
for i in range(len(digit_list)):
    pengira = input_string.count(digit_list[i])
    print(i, pengira)