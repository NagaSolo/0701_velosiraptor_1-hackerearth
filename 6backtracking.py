""" N-Queens
Given a chess board having N cells, you need to place N queens on the board in such a way that no queen attacks any other queen.

board must be NxN, queens must be N

Input:
The only line of input consists of a single integer denoting N.

Sample Input:
4

Sample Output:
YES
0 1 0 0 
0 0 0 1 
1 0 0 0 
0 0 1 0

Output:
If it is possible to place all the N queens in such a way that no queen attacks another queen, then print "YES" (without quotes) in first line, then print N lines having N integers. The integer in  line and  column will denote the cell  of the board and should be 1 if a queen is placed at  otherwise 0. If there are more than way of placing queens print any of them.

If it is not possible to place all N queens in the desired way, then print "NO" (without quotes).

Constraints:
"""

# print board in matrix format
def output_board(board_matrix):
    for row in board_matrix:
        for column in row:
            print(column, end=' ')
        print()

# get the number (int) of queens
queens = int(input())

board_matrix = [[0]*queens for _ in range(queens)] # ignore the counter using '_'
print(board_matrix)

def under_attack(row, column):
    ''' checked to see if there are queens that can atack row columns'''
    for check in range(0, queens):
        if board_matrix[column][check] == 1 or board_matrix[check][column] == 1:
            return True
    # queen can attack diagonally, check diagonal attack
    for check in range(0, queens):
        for check_diagonal in range(0,queens):
            if (check_diagonal + check == row + column) or (check_diagonal - check == row - column):
                if board_matrix[check][check_diagonal] == 1:
                    return True
    return False

def queens_placement(queen):
    # if queens == 0, just return true
    if queen == 0:
        return True
    for row in range(0, queens):
        for column in range(0, queens):
            ''' check state of square, queen can't be placed if square is occupied or under attacked'''
            if (not(under_attack(row, column))) and (board_matrix[row][column] != 1):
                board_matrix[row][column] = 1 # place the queen
                # recursion to check next queens placement
                if queens_placement(queen - 1) == True:
                    return True
                board_matrix[row][column] = 0
    return False

# cannot print matrices in matrix format yet
queens_placement(queens)
for square in board_matrix:
    print(square)