""" Problem Statement

    Factorial of a number n can be defined as product of all positive numbers less than or equal to n. 
    It the multiplying sequence of numbers in a descending order till 1. It is defined by the symbol of exclamation (!).

    1!=1

    2!=2×1

    3!=3×2×1

    4!=4×3×2×1
    ...
    n! = n×(n−1)×(n−2)...×2×1

    n!=n×(n−1)×(n−2)...×2×1

    Factorial of 0 is 1, that is, 0! = 1.

"""
def factorial(number):
    if number == 0 or number == 1:
        print(1)
    else:
        return number * factorial(number - 1)

number_choice = int(input('Input a number: '))

print(factorial(number_choice))